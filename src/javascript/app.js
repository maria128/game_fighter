import FightersView from './fightersView';
import { fighterService } from './services/fightersService';
import Fighter from './services/fighter';

class App {
  constructor() {
    this.startApp();
  }

  static rootElement = document.getElementById('root');
  static loadingElement = document.getElementById('loading-overlay');

  async startApp() {
    try {
      App.loadingElement.style.visibility = 'visible';
      
      const fighters = await fighterService.getFighters();

      const fightersView = new FightersView(fighters, fighterService);
      const fightersElement = fightersView.element;
      fightersView.rootElement = App.rootElement;
      App.rootElement.appendChild(fightersElement);

    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }
  static fight (player1, player2) {
    do {
      let power1 = player1.getHitPower() - player2.getBlockPower();

      if(power1 > 0) {

        player2.health = player2.health - power1;
      }
      let power2 = player2.getHitPower() - player1.getBlockPower();

      if(power2 > 0) {
        player1.health = player1.health - power2;
      }

    } while (player2.health > 0 && player1.health > 0);

    if(player2.health <= 0) {
      this.winner = player1.name;
    } else if(player1.health  <= 0 ) {
      this.winner = player2.name;
    }

    return this.winner;

  }
  async test() {
    const ar1 = await this.loadInfo(1);
    const ar2 = await this.loadInfo(2);

    const player1 = new Fighter(ar1);
    const player2 = new Fighter(ar2);
    App.fight(player1, player2);
  }

}

export default App;