import View from './view';

class FighterView extends View {
    constructor(fighter, handleClick) {
        super();

        this.createFighter(fighter, handleClick);
    }

    createFighter(fighter, handleClick) {
        const {name, source} = fighter;
        const nameElement = this.createName(name);
        const imageElement = this.createImage(source);

        this.element = this.createElement({tagName: 'div', className: 'fighter'});
        this.element.append(imageElement, nameElement);
        this.element.addEventListener('click', event => handleClick(event, fighter), false);
    }

    createName(name) {
        const nameElement = this.createElement({tagName: 'span', className: 'name'});
        nameElement.innerText = name;

        return nameElement;
    }

    createImage(source) {
        const attributes = {src: source};
        const imgElement = this.createElement({
            tagName: 'img',
            className: 'fighter-image',
            attributes
        });
        return imgElement;
    }

    createModal(fighter, handleClick) {
        const {name, health, attack, defense} = fighter;

        const modal = this.createElement({tagName: 'div', className: 'modal'});

        const span = this.createElement({tagName: 'span', className: 'close'});
        span.innerHTML = '&times;';

        const form = this.createElement({tagName: 'form', className: 'modal-content'});
        const h2 = this.createElement({tagName: 'h2', className: 'modal-title'});
        h2.innerText = name;

        const container = this.createElement({tagName: 'div', className: 'container'});

        this.elementLabel = this.createLabel('userName', 'user name');
        this.elementInput = this.createInput(name, 'text');

        container.append(this.elementLabel, this.elementInput);

        this.elementLabel = this.createLabel('health', 'user health');
        this.elementInput = this.createInput(health, 'text');

        container.append(this.elementLabel, this.elementInput);

        this.elementLabel = this.createLabel('attack', 'user attack');
        this.elementInput = this.createInput(attack, 'text');

        container.append(this.elementLabel, this.elementInput);

        this.elementLabel = this.createLabel('defense', 'user defense');
        this.elementInput = this.createInput(defense, 'text');

        const attrSubmit = {type: 'submit'};
        const submit = this.createElement({tagName: 'button', className: 'submit', attrSubmit});
        submit.innerText = 'Изменить';
        container.append(this.elementLabel, this.elementInput, submit);
        form.append(h2);
        form.append(span);
        form.append(container);
        modal.append(form);
        span.addEventListener('click', event => handleClick(event), false);

        return modal;
    }

    createInput(name, type) {
        const attributes = {name: 'name', type: type, value: name};
        const elementInput = this.createElement({tagName: 'input', className: 'input', attributes});

        return elementInput;
    }

    createLabel(attrFor, text) {
        const attributes = {for: 'attrFor'};
        const elementLabel = this.createElement({tagName: 'label', className: 'label', attributes});
        elementLabel.innerText = text;
        return elementLabel
    }


}

export default FighterView;