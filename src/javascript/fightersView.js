import View from './view';
import FighterView from './fighterView';

class FightersView extends View {
  constructor(fighters, fighterService) {
    super();
    this.fighterService = fighterService;
    this.handleClick = this.handleFighterClick.bind(this);
    this.handleClose = this.handleClickClose.bind(this);
    this.rootElement ='';
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });
    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  handleFighterClick(event, fighter) {

    if(this.fightersDetailsMap.size === 0 || !this.fightersDetailsMap.get(fighter._id)) {
      this.fightersDetailsMap.set(fighter._id, fighter);
    }
    if(typeof (this.fightersDetailsMap.get(fighter._id)).health === 'undefined') {
      this.loadInfo(fighter._id);
    }
    const fighterView = new FighterView(this.fightersDetailsMap.get(fighter._id), this.handleClick);
    this.rootElement.append(fighterView.createModal(this.fightersDetailsMap.get(fighter._id), this.handleClose));
  }

  handleClickClose(event) {
    const target = event.target;
    const parent = target.parentElement.parentElement;
    parent.remove();
  }

  async loadInfo(id) {
    try {
      const getDeteilsFighter = await this.fighterService.getFighterDetails(id);
      const fighterElements = [getDeteilsFighter].map(fighter => {
        this.fightersDetailsMap.delete(fighter._id);
        this.fightersDetailsMap.set(fighter._id, fighter);
      });
      return getDeteilsFighter;
    } catch (error) {
      throw error;
    }
  }
}

export default FightersView;