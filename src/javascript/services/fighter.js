class Fighter {
    constructor(fighter) {
        this.name =  fighter.name;
        this.health =  fighter.health;
        this.attack =  fighter.attack;
        this.defense =  fighter.defense;
    }

    getName() {
        return this.name
    }
    getHealth() {
        return this.health
    }

    getAttack() {
        return this.attack
    }

    getDefense() {
        return this.defense
    }
    getHitPower() {
        const dodgeChance = Math.floor(Math.random() * 2) + 1;
        this.power = this.attack * dodgeChance;
        return this.power;
    }
    getBlockPower() {
        const criticalHitChance = Math.floor(Math.random() * 2) + 1;
        this.power = this.defense * criticalHitChance;
        return this.power;

    }
}

export default Fighter;